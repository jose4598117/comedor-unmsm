<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $primaryKey = 'dia';
    public $timestamps = false;
    public $incrementing = false;
}
