@extends('layouts.masterAlumno')

@section('content')

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">


            <ul class="list-unstyled components">
                <p></p>
                <div></div>
                <li>
                    <a href="http://localhost:8000/validar-ticket">Validar Ticket</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/actualizar-menu">Actualizar Menú</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/actualizar-horarios">Actualizar Horarios</a>
                </li>
                <div></div>
                <li class ="active">
                    <a href="http://localhost:8000/suspender-alumno">Suspender Alumno</a>
                </li>
            </ul>

        
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <h1 class="display-5"><center><b>Suspender Alumno</b></center></h1>

            <div class="line"></div>
                <div class="container">
        <center>
        <p>Aquí podrá suspender permanentemente al alumno. Recuerda ingresar el codigo y la razón de bloqueo.&nbsp;</p>
        <form method = "POST" action="/action">
            @csrf
            <div class="form-group row">
                <label for="inputCodVer"></label>
                <input type="CodVer" name="codigoAlumno" class="form-control" id="inputCodVer" placeholder="Codigo del Alumno" required>
                &nbsp
                <input type="text" name="razonInhabilitar" class="form-control" id="inputRazon" placeholder="Razón" required>
            </div>
            </center>
            <div class="container">
                <div class="row">
                    <div class="col-sm-center">
                        <button type="submit" class="btn btn-danger" name="option" value="inhabilitar">Suspender</button> 
                        <button type="submit" class="btn btn-success" name="option" value="habilitar">Habilitar</button> 
                    </div>
                </div>
            </div>
        </form>
        </div>
            <div class="line"></div>
            <h3 class="display-5"><center>Lista de suspendidos</center></h3>
            &nbsp
            <div class="table-responsive">
            <table class="table table-hover">
                <thead class="thead-dark">
                    <th scope="col">Código</th>
                    <th scope="col">Nombre de alumno</th>
                    <th scope="col">Razon de suspensión</th>
                </thead>
                <tbody>
                    @foreach($alumnos as $alumno)
                    @if($alumno->active == false)
                    <tr class="table-light">
                        <td>{{$alumno->id}}</td>
                        <td>{{$alumno->name}}</td>
                        <td>{{$alumno->razon_active}}</td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
@stop