<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    public $timestamps = false;

    public function user(){
        return $this->hasMany('Ticket', 'turno_id');
    }
}
