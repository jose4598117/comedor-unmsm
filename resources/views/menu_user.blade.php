@extends('layouts.masterAlumno')

@section('content')
<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">

            <ul class="list-unstyled components">
                <li class="active">
                    <a href="http://localhost:8000/menu">Menú Semanal</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/solicitar-ticket">Solicitar Ticket</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/visualizar-ticket">Visualizar Ticket</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/historial-ticket">Historial de Tickets</a>
                </li>
            </ul>


        </nav>

        <!-- Page Content  -->
        <div id="content">


            <h1 class="display-5"><center><b>Menú Semanal</b></center></h1>

            <div class="line"></div>
            <center><p>Bienvenido al sistema de tickets del comedor de la UNMSM. Esta semana tenemos esto para ti.</p></center>
            <center><p>{{$fecha}}</p></center>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col"></th>
                            @foreach($horarios as $horario)
                                <th scope="col">{{$horario->dia}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="table-light">
                            <th scope="row">Entrada</th>
                                @foreach($horarios as $horario)
                                    <td>{{$horario->entrada}}</td>
                                @endforeach
                        </tr>
                        <tr class="table-light">
                            <th scope="row">Segundo</th>
                                @foreach($horarios as $horario)
                                    <td>{{$horario->almuerzo}}</td>
                                @endforeach
                        </tr>
                        <tr class="table-light">
                            <th scope="row">Postre</th>
                                @foreach($horarios as $horario)
                                    <td>{{$horario->postre}}</td>
                                @endforeach
                        </tr>
                    </tbody>
                </table>
            </div>

</div>

<!-- jQuery CDN - Slim version (=without AJAX) -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Popper.JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>
</body>
@stop