<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Ticket;

class cancelarController extends Controller
{
    public function showView(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 1)
                return view('cancelar_user');
            else{
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';   
                return view('mensaje_alumno', compact('cabecera','mensaje')); //Vista de no eres el usuario
            }
        }
        catch(\Exception $e){
            return view('welcome'); //No estás logeado
        }
    }

    public function cancelar(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 1){
                try{
                    //Información desde el metodo POST
                    $idTicket = $request->id;
                    $idUser = $request->user()->id;
        
                    //Validar si el ticket existe
                    $ticket = Ticket::find($idTicket);
                    $ticket->id;
        
                    //Obtiene el id del ultimo ticket
                    $last_ticket = Ticket::where('user_id', $idUser)->latest()->first();
                    $last_idTicket = $last_ticket->id;
        
                    //Si el idTicket que escribió es el ultimo ticket del usuario
                    if($idTicket == $last_idTicket){
                        if($ticket->flag_activo == true){
                            if(date('a', time()) == 'pm'){
                                $user = User::find($ticket->user_id);
        
                                $ticket->flag_activo = false;
                                $ticket->save();

                                $ticket->flag_cancelado = true;
                                $ticket->save();
                    
                                $user->hasCurrentTicket = false;
                                $user->save();
    
                                $cabecera = 'Mensaje';
                                $mensaje = 'El ticket ' . $idTicket . ' ha sido cancelado';   
                                return view('mensaje_alumno', compact('cabecera','mensaje'));
                            }
                            else{
                                $cabecera = 'Error';
                                $mensaje = 'Estás fuera de hora.';   
                                return view('mensaje_alumno', compact('cabecera','mensaje'));
                            }
                        }
                        else{
                            $cabecera = 'Error';
                            $mensaje = 'El ticket ingresado ya está cancelado.';   
                            return view('mensaje_alumno', compact('cabecera','mensaje'));
                        }
                    }
                    else{
                        $cabecera = 'Error';
                        $mensaje = 'El ticket ingresado no te pertenece.';   
                        return view('mensaje_alumno', compact('cabecera','mensaje'));
                    }
                }  
                catch (\Exception $e){
                    $cabecera = 'Error';
                    $mensaje = 'El ticket no existe. Vuelvalo a intentar.';   
                    return view('mensaje_alumno', compact('cabecera','mensaje'));
                }
            }
            else{
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';   
                return view('mensaje_operador', compact('cabecera','mensaje'));
            }   
        }
        catch(\Exception $e){
            return view('welcome'); //No estás logeado
        }
    }
}
