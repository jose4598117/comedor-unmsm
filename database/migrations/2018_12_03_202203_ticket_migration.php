<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TicketMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turnos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->time('entrada');
            $table->time('salida');
            $table->integer('cantidad')->unsigned();
            $table->integer('restante')->unsigned();
        });
        
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('turno_id')->unsigned();
            $table->datetime('date');
            $table->boolean('flag_activo');
            $table->boolean('flag_cancelado');
            $table->boolean('flag_strike');
            $table->timestamps();
        });

        Schema::table('tickets', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('turno_id')->references('id')->on('turnos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turnos');
        Schema::dropIfExists('tickets');
    }
}
