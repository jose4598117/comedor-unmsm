@extends('layouts.masterAlumno')

@section('content')

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">


            <ul class="list-unstyled components">
                <p></p>
                <div></div>
                    <li class="active">
                    <a href="http://localhost:8000/validar-ticket">Validar Ticket</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/actualizar-menu">Actualizar Menú</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/actualizar-horarios">Actualizar Horarios</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/suspender-alumno">Suspender Alumno</a>
                </li>
            </ul>

        
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <h1 class="display-5"><center><b>Validar Ticket</b></center></h1>

            <div class="line"></div>
            <div class="container">
                <center>
                <p>Aquí puedes validar el ticket de manera manual.</p>
                <form method = "POST" action="/check">
                    @csrf
                <div class="form-group row">
                    <label for="inputCodVer" class="col-sm-2 col-form-label"></label>
                    <input type="CodVer" name="codigoTicket" class="form-control" id="inputCodVer" placeholder="Codigo de Ticket">                    
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                    <button type="submit" class="btn btn-danger">Validar</button> 
                    </div>
                </div>
                </form>
                </center>
                </div>
            <div class="line"></div>
        </div>
    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
@stop