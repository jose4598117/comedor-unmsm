<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Ticket;
use App\User;

class inhabilitarController extends Controller
{
    public function showView(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 2){
                $alumnos = User::all();
                return view('inhabilitar_alumno', compact('alumnos'));
            } 
            else{
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';   
                return view('mensaje_alumno', compact('cabecera','mensaje'));
            }    
        }
        catch(\Exception $e){
            return view('welcome'); //Vista de no estás logeado
        }
    }

    public function action(Request $request){
        $option = $request->option;
        if($option == "inhabilitar"){
            try{
                $role = $request->user()->role_id;
                if($role == 2)
                    {
                        //Desde el Request
                        $idUser = $request->codigoAlumno;
    
                        try{
                            $alumno = User::find($idUser);
                            //Revisamos si el alumno está activo o no
                            if($alumno->active == false){
                                $cabecera = 'Error';
                                $mensaje = 'El alumno ya fue inhabilitado.';   
                                return view('mensaje_operador', compact('cabecera','mensaje'));
                            }
                            else{
                                $user = User::find($idUser);
                                $user->active = false;
                                $user->hasCurrentTicket = 0;
                                $user->razon_active = $request->razonInhabilitar;
                                $user->save();
    
                                $cabecera = 'Mensaje';
                                $mensaje = 'El alumno ha sido inhabilitado con éxito.';   
                                return view('mensaje_operador', compact('cabecera','mensaje'));
                            }
                        }
                        catch (\Exception $e){
                            $cabecera = 'Error';
                            $mensaje = 'No existe ningun alumno con este codigo';   
                            return view('mensaje_operador', compact('cabecera','mensaje'));
                        }    
                    }
                else{   
                    $cabecera = 'Error';
                    $mensaje = 'No cuentas con accesos suficientes para acceder aquí';   
                    return view('mensaje_alumno', compact('cabecera','mensaje'));
                }
            }
            catch(\Exception $e){
                return view('welcome'); //Vista de no estás logeado
            }
        }
        else{
            try{
                $role = $request->user()->role_id;
                if($role == 2)
                    {
                        //Desde el Request
                        $idUser = $request->codigoAlumno;
    
                        try{
                            $alumno = User::find($idUser);
                            //Revisamos si el alumno está activo o no
                            if($alumno->active == true){
                                $cabecera = 'Error';
                                $mensaje = 'El alumno se encuentra habilitado.';   
                                return view('mensaje_operador', compact('cabecera','mensaje'));
                            }
                            else{
                                $user = User::find($idUser);
                                $user->active = true;
                                $user->hasCurrentTicket = 0;
                                $user->razon_active = $request->razonInhabilitar;
                                $user->save();
    
                                $cabecera = 'Mensaje';
                                $mensaje = 'El alumno ha sido habilitado con éxito.';   
                                return view('mensaje_operador', compact('cabecera','mensaje'));
                            }
                        }
                        catch (\Exception $e){
                            $cabecera = 'Error';
                            $mensaje = 'No existe ningun alumno con este codigo';   
                            return view('mensaje_operador', compact('cabecera','mensaje'));
                        }    
                    }
                else{   
                    $cabecera = 'Error';
                    $mensaje = 'No cuentas con accesos suficientes para acceder aquí';   
                    return view('mensaje_alumno', compact('cabecera','mensaje'));
                }
            }
            catch(\Exception $e){
                return view('welcome'); //Vista de no estás logeado
            }
        }
    }

    public function inhabilitar(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 2)
                {
                    //Desde el Request
                    $idUser = $request->codigoAlumno;

                    try{
                        $alumno = User::find($idUser);
                        //Revisamos si el alumno está activo o no
                        if($alumno->active == false){
                            $cabecera = 'Error';
                            $mensaje = 'El alumno ya fue inhabilitado.';   
                            return view('mensaje_operador', compact('cabecera','mensaje'));
                        }
                        else{
                            $user = User::find($idUser);
                            $user->active = false;
                            $user->hasCurrentTicket = 0;
                            $user->razon_active = $request->razonInhabilitar;
                            $user->save();

                            $cabecera = 'Mensaje';
                            $mensaje = 'El alumno ha sido inhabilitado con éxito.';   
                            return view('mensaje_operador', compact('cabecera','mensaje'));
                        }
                    }
                    catch (\Exception $e){
                        $cabecera = 'Error';
                        $mensaje = 'No existe ningun alumno con este codigo';   
                        return view('mensaje_operador', compact('cabecera','mensaje'));
                    }    
                }
            else{   
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';   
                return view('mensaje_alumno', compact('cabecera','mensaje'));
            }
        }
        catch(\Exception $e){
            return view('welcome'); //Vista de no estás logeado
        }
    }

    public function habilitar(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 2)
                {
                    //Desde el Request
                    $idUser = $request->codigoAlumno;

                    try{
                        $alumno = User::find($idUser);
                        //Revisamos si el alumno está activo o no
                        if($alumno->active == true){
                            $cabecera = 'Error';
                            $mensaje = 'El alumno se encuentra habilitado.';   
                            return view('mensaje_operador', compact('cabecera','mensaje'));
                        }
                        else{
                            $user = User::find($idUser);
                            $user->active = true;
                            $user->hasCurrentTicket = 0;
                            $user->razon_active = $request->razonInhabilitar;
                            $user->save();

                            $cabecera = 'Mensaje';
                            $mensaje = 'El alumno ha sido habilitado con éxito.';   
                            return view('mensaje_operador', compact('cabecera','mensaje'));
                        }
                    }
                    catch (\Exception $e){
                        $cabecera = 'Error';
                        $mensaje = 'No existe ningun alumno con este codigo';   
                        return view('mensaje_operador', compact('cabecera','mensaje'));
                    }    
                }
            else{   
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';   
                return view('mensaje_alumno', compact('cabecera','mensaje'));
            }
        }
        catch(\Exception $e){
            return view('welcome'); //Vista de no estás logeado
        }
    }

}
