@extends('layouts.masterAlumno')

@section('content')

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">

            <ul class="list-unstyled components">
                <li>
                    <a href="http://localhost:8000/menu">Menú Semanal</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/solicitar-ticket">Solicitar Ticket</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/visualizar-ticket">Visualizar Ticket</a>
                </li>
                <div></div>
                <li  class = "active">
                    <a href="http://localhost:8000/historial-ticket">Historial de Tickets</a>
                </li>
            </ul>


        </nav>


        <!-- Page Content  -->
        <div id="content">

           

            <h1 class="display-5"><center><b>Historial de Tickets</b></center></h1>

            <div class="line"></div>
            <center><p>Recordar es volver a vivir. ¿Cierto?</p></center>
            <center><p>Número de strikes: {{App\User::find(Auth::user()->id)->strikes}}</p></center>
<div class="table-responsive">
    <table class="table table-hover">
        <thead class="thead-dark">
            <tr class="centrados">
            <th scope="col">ID ticket</th>
            <th scope="col">Código alumno</th>
            <th scope="col">Nombre</th>
            <th scope="col">Fecha de solicitud</th>
            <th scope="col">Turno</th>
            <th scope="col">Estado</th>
            </tr>
        </thead>
        <tbody align="center">
            @foreach($tickets as $ticket)
                @if($ticket->user_id == Auth::user()->id)
                    <tr class="table-light">
                    <td>{{$ticket->id}}</td>
                    <td>{{$ticket->user_id}}</td>
                    <td>{{App\User::find($ticket->user_id)->name}}</td>
                    <td>{{$ticket->date}}</td>
                    <td>{{App\Turno::find($ticket->turno_id)->nombre}}</td>
                    @if($ticket->flag_activo == true)
                        <td>No canjeado</td>
                    @else
                        @if($ticket->flag_cancelado == true)
                        <td><b>Cancelado</b></td>
                        @else
                            @if($ticket->flag_strike == true)
                            <td><b>Strike</b></td>
                            @else
                            <td><b>Canjeado</b></td>
                            @endif
                        @endif
                    @endif
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>

    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
@stop