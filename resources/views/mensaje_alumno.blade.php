@extends('layouts.masterAlumno')

@section('content')

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">

            <ul class="list-unstyled components">
                <li >
                    <a href="http://localhost:8000/menu">Menú Semanal</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/solicitar-ticket">Solicitar Ticket</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/visualizar-ticket">Visualizar Ticket</a>
                </li>
                <div></div>
                <li>
                    <a href="http://localhost:8000/historial-ticket">Historial de Tickets</a>
                </li>
            </ul>


        </nav>

        <!-- Page Content  -->
        <div id="content">
            
            <h1 class="display-5"><center><b>{{$cabecera}}</b></center></h1>

            <div class="line"></div>
            <div class="container">
                <center><h5>{{$mensaje}}</h5></center>
            </div>
            <div class="line"></div>
                <center>
                <form action= {{url()->previous()}}>
                    <button type="submit" class="btn btn-danger">Regresar a la página anterior</button>
                </form>
                </center>            
        </div>
    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
@stop
