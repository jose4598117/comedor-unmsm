<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('index');

//Menu
Route::get('/menu', 'MenuController@show')->name('show');

//Solicitud Ticket
Route::get('/solicitar-ticket', 'TicketController@solicitud')->name('solicitar-ticket');
Route::post('/crear', 'TicketController@crearticket')->name('crearticket');

//Visualizar Ticket
Route::get('/visualizar-ticket', 'VisualizarController@visualizar')->name('visualizar-ticket');

//Cancelar Ticket
//Route::get('/cancelar-ticket', 'CancelarController@showView')->name('cancelar-ticket');
Route::post('/cancelar', 'CancelarController@cancelar')->name('cancelar');

//Historial tickets
Route::get('/historial-ticket', 'HistorialController@historial')->name('historial-ticket');

//Validar Ticket
Route::post('/check', 'ValidarController@check')->name('check');
Route::get('/validar-ticket', 'ValidarController@validar')->name('validar');
Route::get('/check-url/{idTicket}', 'ValidarController@check_url')->name('check_url');

//Cargar Strikes
Route::get('/cargar-strikes', 'StrikesController@showView')->name('showView');
Route::post('/strikes', 'StrikesController@cargarStrikes')->name('cargarStrikes');

//Actualizar Raciones
Route::get('/actualizar-raciones', 'RacionesController@showView')->name('showView');
Route::post('/actualizar-turno', 'RacionesController@actualizarTurno')->name('actualizarTurno');
Route::post('/actualizar-turnos', 'RacionesController@actualizarTurnos')->name('actualizarTurnos');

//Actualizar Horarios
Route::get('/actualizar-horarios', 'TurnosController@showView')->name('showView');
Route::post('/guardar-horarios', 'TurnosController@guardarHorarios')->name('guardarHorarios');

//Actualizar Menu
Route::get('/actualizar-menu', 'MenuController@showEditar')->name('showEditar');
Route::post('/guardar-menu', 'MenuController@guardarMenu')->name('guardarMenu');

//Inhabilitar Alumno
Route::get('/suspender-alumno', 'InhabilitarController@showView')->name('showView');
Route::post('/inhabilitar', 'InhabilitarController@inhabilitar')->name('inhabilitar');
Route::post('/habilitar', 'InhabilitarController@habilitar')->name('habilitar');
Route::post('/action', 'InhabilitarController@action')->name('action');

//Pruebas
Route::get('/location', 'LocationController@showView')->name('showView');

Route::post('/getpos', 'TestController@getpos')->name('getpos');
Route::get('/test_post', 'TestController@test_post')->name('test_post');


Route::get('/test', 'TestController@test')->name('test');
Route::get('/test/crear', 'MenuController@crearmenu')->name('crearmenu');
Route::get('/test/{dia}', 'MenuController@detallemenu')->name('detallemenu');
Route::get('/max', 'MenuController@getmax')->name('getmax');

//Error
Route::get('/error', 'ErrorController@showView')->name('showView');

//QR

Route::get('qr-code-g', function () {
    \QrCode::size(500)
              ->format('png')
              ->generate('ItSolutionStuff.com', public_path('images/qrcode.png'));
      
    return view('qrCode');
      
});
