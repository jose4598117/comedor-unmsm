<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class strikesController extends Controller
{   
    public function showView(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 2)
                return view('cargar_strikes');
            else   
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';   
                return view('mensaje_alumno', compact('cabecera','mensaje')); //Vista de no eres el usuario
        }
        catch(\Exception $e){
            return view('welcome'); //Vista de no estás logeado
        }
    }

    public function cargarStrikes(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 2)
                if(date('a', time()) == 'pm' and date('h', time())>=6){
                    User::where('hasCurrentTicket', 1)->increment('strikes');
                    $cabecera = 'Mensaje';
                    $mensaje = 'Strikes cargados correctamente cargados';   
                    return view('mensaje_operador', compact('cabecera','mensaje'));
                }
                else{
                    $cabecera = 'Mensaje';
                    $mensaje = 'Aún no puedes cargar los strikes';   
                    return view('mensaje_operador', compact('cabecera','mensaje'));
                }
            else{
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';   
                return view('mensaje_alumno', compact('cabecera','mensaje')); //Vista de no eres el usuario
            }   
        }
        catch(\Exception $e){
            return view('welcome'); //No estás logeado
        }
    }
}
